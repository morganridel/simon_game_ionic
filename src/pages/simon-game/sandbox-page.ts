import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'sandbox-page',
  templateUrl: 'sandbox-page.html'
})
export class SandboxPage {

  constructor(public navCtrl: NavController) {

  }

}
