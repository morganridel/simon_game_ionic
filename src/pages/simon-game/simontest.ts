import { Component } from '@angular/core';

import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-simontest',
  templateUrl: 'simontest.html'
})
export class SimonTest {

  constructor(public navCtrl: NavController) {

  }

}
