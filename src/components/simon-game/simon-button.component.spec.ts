import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By }           from '@angular/platform-browser';
import { SimonButton } from './simon-button.component';
import { IonicModule, Platform, NavController} from 'ionic-angular/index';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PlatformMock, StatusBarMock, SplashScreenMock } from '../../../test-config/mocks-ionic';
import { COLORS } from './simon-colors';

describe('Component SimonButton', () => {
  let comp: SimonButton;
  let fixture: ComponentFixture<SimonButton>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SimonButton],
      imports: [
        IonicModule.forRoot(SimonButton)
      ],
      providers: [
        NavController,
        { provide: Platform, useClass: PlatformMock},
        { provide: StatusBar, useClass: StatusBarMock },
        { provide: SplashScreen, useClass: SplashScreenMock },
      ]
    });
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimonButton);
    comp = fixture.componentInstance;
  });

  it('should create component', () => expect(comp).toBeDefined());

  it('doit être constitué d\'un unique bouton', () => {
    expect(fixture.debugElement.queryAll(By.css('*[ion-button]')).length).toBe(1);
    expect(fixture.debugElement.query(By.css('button'))).toBeTruthy();
  });

  it('doit être de la couleur indiquée en entrée', () => {
    let expectedColor = COLORS[1].name;
    comp.color = expectedColor;
    fixture.detectChanges();
    expect(comp.getHexColorCode(comp.color)).toBe(COLORS[1].code);
  });
  it('doit lancer la fonction d\'animation après un clic', () => {
    let button = fixture.debugElement.query(By.css('button'));
    let spy = spyOn(comp, 'animer');
    fixture.detectChanges();
    button.triggerEventHandler('click', null);
    expect(spy).toHaveBeenCalled();
  });

});
