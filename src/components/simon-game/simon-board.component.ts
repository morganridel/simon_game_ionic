import { Component, ViewChildren } from '@angular/core';
import { COLORS } from './simon-colors';
import { SimonButton } from './simon-button.component';


@Component({
  selector: 'simon-board',
  templateUrl: 'simon-board.html',
})
export class SimonBoard {
  colors = COLORS;
  texteEcran: any = "0/1";
  score: any = 0;
  record: number = 0;
  dureeAnimation: number = 1000;
  emplacementDansLaSerie: number = 0;
  @ViewChildren(SimonButton) buttons;

  //Une serie est un tableau de couleur correspondant a un bouton
  serieCorrecte : string[] = [];
  serieJoueur: string[] = [];
  locked: boolean = true;
  jeuEnCours: boolean = false;
  ecranAllume: boolean = false;

  constructor() {

  }

  ngAfterViewInit() {
    this.buttons = this.buttons.toArray();
    this.lockButtons();

  }

  ajouterCouleurAleatoireDansSerieCorrecte() {
    let couleurAleatoire = this.getCouleurAleatoire();
    this.serieCorrecte.push(couleurAleatoire);
  }

  getCouleurAleatoire() : string {
    let colorNumber = Math.floor(Math.random()*this.colors.length);
    return this.colors[colorNumber].name;
  }

  ajouterCouleurDansSerieJoueur(couleur: string) {
    if (this.couleurEstValide(couleur)) {
      this.serieJoueur.push(couleur);
    }
    else {
      throw new TypeError("Couleur non valide !");
    }
  }

  couleurEstValide(couleur: string) : boolean {
    let valide = false;
    for (let coul of this.colors) {
      if (coul.name == couleur) {
        valide = true;
      }
    }
    return valide;
  }

  animerSerieCorrecte(callback?: () => void) {
    if (this.emplacementDansLaSerie < this.serieCorrecte.length) {
      this.animerBoutonParCouleur(this.serieCorrecte[this.emplacementDansLaSerie]);
      this.emplacementDansLaSerie++;
      setTimeout(() => { this.animerSerieCorrecte(callback) }, this.dureeAnimation+500);
    }
    else {
      this.emplacementDansLaSerie = 0;
      callback();
    }
  }

  animerBoutonParCouleur(couleur: string) {
    if (this.couleurEstValide(couleur)) {
      for (let bouton of this.buttons) {
        if (bouton.color == couleur) {
          bouton.animer(this.dureeAnimation);
        }
      }
    }
    else {
      throw new TypeError("Couleur non valide !");
    }
  }

  lockButtons() {
    this.locked = true;
    for (let bouton of this.buttons) {
      bouton.lockButton();
    }
  }

  unlockButtons() {
    this.locked = false;
    for (let bouton of this.buttons) {
      bouton.unlockButton();
    }
  }

  joueurClique(couleur: string) {
    if (!this.locked) {
      this.ajouterCouleurDansSerieJoueur(couleur);

      let couleurSerie = this.serieCorrecte[this.emplacementDansLaSerie];

      if (this.couleursSontEgales(couleur, couleurSerie)) {
        this.emplacementDansLaSerie++;
        this.texteEcran = this.emplacementDansLaSerie + "/" + (this.score+1);
        if (this.emplacementDansLaSerie == this.serieCorrecte.length) {
          this.lockButtons();
          this.score++;
          if (this.score > this.record) {
            this.setRecord(this.score);
          }
          setTimeout(() => { this.eteindreBoutons(); }, 60);
          this.animationCorrecte(this.dureeAnimation*2);
          setTimeout(() => { this.tourDeLOrdinateur(); }, this.dureeAnimation*2+500); //on attend la fin de l'animation des boutton et de la victoire + un petit temps
        }
      }
      else {
        this.partiePerdue();
      }
    }
  }

  couleursSontEgales(couleur1: string, couleur2: string) {
    if (!this.couleurEstValide(couleur1) || !this.couleurEstValide(couleur2)) {
      throw new TypeError("Couleur non valide !");
    }
    else {
      return couleur1 == couleur2;
    }
  }

  partiePerdue() {
    this.emplacementDansLaSerie = 0;
    //alert("Perdu !");
    this.serieCorrecte.length = 0;
    this.serieJoueur.length = 0;
    this.score = 0;
    this.texteEcran = "0/" + (this.score+1);
    this.lockButtons();
    this.animationDefaite(this.dureeAnimation);
    setTimeout(() => {
      this.eteindreEcran();
      this.jeuEnCours = false;
    }, this.dureeAnimation);
  }

  tourDeLOrdinateur() {
    this.emplacementDansLaSerie = 0;
    this.lockButtons();
    this.ajouterCouleurAleatoireDansSerieCorrecte();
    this.animerSerieCorrecte(() => { this.unlockButtons() });

  }

  lancerJeu() {
    this.jeuEnCours = true;
    this.allumerEcran();
    this.tourDeLOrdinateur();
  }

  setRecord(score: number) {
    this.record = score;
  }

  allumerEcran() {
    this.ecranAllume = true;
  }

  eteindreEcran() {
    this.ecranAllume = false;
  }

  eteindreBoutons() {
    for (let bouton of this.buttons) {
      bouton.eteindre();
    }
  }

  animationCorrecte(temps: number) {
    this.texteEcran = ":)";
    setTimeout(() => { this.texteEcran = ""; }, temps/4);
    setTimeout(() => { this.texteEcran = ":)"; }, (2*temps)/4);
    setTimeout(() => { this.texteEcran = ""; }, (3*temps)/4);
    setTimeout(() => { this.texteEcran = this.texteEcran = "0/" + (this.score+1); }, temps);
  }

  animationDefaite(temps: number) {
    this.texteEcran = ":(";
    for (let bouton of this.buttons) {
      bouton.animer((2*temps)/5);
    }
    setTimeout(() => {
      for (let bouton of this.buttons) {
        bouton.animer((2*temps)/5);
      }
    }, (3*temps)/5);
    setTimeout(() => { this.texteEcran = this.texteEcran = "0/" + (this.score+1);; }, temps);
  }

  tapped(event) {
    //il semblerai que l'event tap envoie un event mousedown/click, ce qui est problématique sur mobile
    event.preventDefault();
  }




}
