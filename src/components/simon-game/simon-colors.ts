export const COLORS: any[] = [
  { name: "vert", code: '#2B9C6A' },
  { name: "rouge", code: '#cd1439' },
  { name: "jaune", code: '#f58f06' },
  { name: "bleu", code: '#045ff4' },

];
