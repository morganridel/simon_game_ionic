import { Component, Input } from '@angular/core';
import { COLORS } from './simon-colors';


@Component({
  selector: 'simon-button',
  templateUrl: 'simon-button.html',
})
export class SimonButton {
  @Input() color : String;
  allume : boolean = false;
  timer;
  locked: boolean = false;

  constructor() {

  }

  getHexColorCode(color : String) {
    for (let colorConst of COLORS) {
      if (colorConst.name == color) {
        return colorConst.code;
      }
    }
    return '#FFFFFF';
  }

  enfoncer() {
    if (!this.locked) {
      this.allumer();
    }
  }

  relacher() {
    if (!this.locked) {
      this.eteindre();
    }
  }

  allumer() {
    this.allume = true;
  }

  eteindre() {
    this.allume = false;
  }

  animer(duree = 1000) {
    clearTimeout(this.timer);
    this.eteindre();
    this.allumer();
    this.timer = setTimeout(() => { this.eteindre(); }, duree);
  }

  lockButton() {
    this.locked = true;
  }

  unlockButton() {
    this.locked = false;
  }


}
