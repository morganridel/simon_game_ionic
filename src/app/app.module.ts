import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { SimonTest } from '../pages/simon-game/simontest';
import { SandboxPage } from '../pages/simon-game/sandbox-page';
import { SimonButton } from '../components/simon-game/simon-button.component';
import { SimonBoard } from '../components/simon-game/simon-board.component';


@NgModule({
  declarations: [
    MyApp,
    SimonTest,
    SandboxPage,
    SimonButton,
    SimonBoard,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {}, { links: [] })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SimonTest,
    SandboxPage,
    SimonButton,
    SimonBoard,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
  ]
})
export class AppModule { }
